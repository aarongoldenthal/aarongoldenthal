import { BskyAgent, RichText } from '@atproto/api';

/**
 * @module new-posts
 *
 * Determine new posts since the last published sitemap and post notifications.
 */

import fs from 'node:fs';

const sitemapFilename = 'sitemap.xml';
const postsFilename = 'posts.json';
const postsThreshold = 3;

/**
 * Get URLs from the sitemap.
 *
 * @returns {string[]} Array of URLs from the sitemap.
 * @static
 * @private
 */
const getSitemapUrls = () => {
    const sitemap = fs.readFileSync(sitemapFilename, 'utf8');
    const urlRegex = /<loc>(?<url>.+\/posts\/.+?)<\/loc>/g;
    return [...sitemap.matchAll(urlRegex)].map((match) => match.groups.url);
};

/**
 * @typedef  {object}   Post
 * @property {string}   date        - The post date.
 * @property {string}   description - The post description.
 * @property {string}   inputPath   - The post input path.
 * @property {string}   outputPath  - The post output path.
 * @property {string}   title       - The post title.
 * @property {string}   url         - The post URL.
 * @property {string[]} tags        - The post hashtags.
 */

/**
 * Get post data for new posts since the last published sitemap.
 *
 * @returns {Post[]} Array of post data for new posts.
 * @throws  {Error}  If the sitemap or posts data are invalid or out of sync.
 * @static
 * @private
 */
const getNewPosts = () => {
    const sitemapUrls = getSitemapUrls();
    const posts = JSON.parse(fs.readFileSync(postsFilename, 'utf8'));
    if (
        sitemapUrls.length === 0 ||
        posts.length === 0 ||
        Math.abs(sitemapUrls.length - posts.length) > postsThreshold
    ) {
        throw new Error(
            'Error: sitemap and posts data are invalid or out of sync'
        );
    }
    return posts.filter((post) => !sitemapUrls.includes(post.url));
};

/**
 * Post status to Mastodon.
 *
 * @param  {Post}  post The post data.
 * @throws {Error}      If the status is not posted successfully.
 * @static
 * @private
 */
const postMastodonStatus = async (post) => {
    const accessToken = process.env.MASTODON_TOKEN;
    const instanceUrl = 'https://fosstodon.org';
    const data = {
        status: `${post.description}\n\n${post.url}\n\n${post.tags.join(' ')}`
    };

    const response = await fetch(`${instanceUrl}/api/v1/statuses`, {
        body: JSON.stringify(data),
        headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json'
        },
        method: 'POST'
    });

    const postStatus = await response.json();
    if (!response.ok) {
        throw new Error(`Error posting Mastadon status: ${postStatus.error}`);
    }
    console.log(`Mastodon status successfully posted (ID: ${postStatus.id})`);
};

/**
 * Post status to Bluesky.
 *
 * @param  {Post}  post The post data.
 * @throws {Error}      If the status is not posted successfully.
 * @static
 * @private
 */
// eslint-disable-next-line max-lines-per-function -- Long data structures
const postBlueskyStatus = async (post) => {
    // Use Bluesky agent based on API complexity
    const agent = new BskyAgent({
        service: 'https://bsky.social'
    });

    try {
        await agent.login({
            identifier: process.env.BSKY_ID,
            password: process.env.BSKY_PASSWORD
        });

        const message = `${post.description}\n\n${post.url}`;
        // Rich formatting in posts, for example links and mentions, must be
        // specified by byte offsets, so use the RichText capabilities to
        // detect them.
        const rt = new RichText({ text: message });
        await rt.detectFacets(agent);

        const postRecord = {
            $type: 'app.bsky.feed.post',
            createdAt: new Date().toISOString(),
            // Cards are not automatically created from OG tags in the link,
            // they must be explicitly added. Note this does not include
            // images, which must be referenced, and separately posted to
            // the API.
            embed: {
                $type: 'app.bsky.embed.external',
                external: {
                    description: post.description,
                    title: post.title,
                    uri: post.url
                }
            },
            facets: rt.facets,
            text: rt.text
        };

        await agent.post(postRecord);
        console.log('Bluesky status successfully posted');
    } catch (error) {
        throw new Error(`Error posting Bluesky status: ${error.message}`);
    }
};

/**
 * Submit a post URL to IndexNow.
 *
 * @param  {Post}  post The post data.
 * @throws {Error}      If the status is not posted successfully.
 * @static
 * @private
 */
const postIndexNow = async (post) => {
    const apiKey = process.env.INDEXNOW_API_KEY;
    const indexNowUrl = 'https://api.indexnow.org/IndexNow';
    const postUrl = new URL(post.url);
    const { host } = postUrl;
    const data = {
        host,
        key: apiKey,
        keyLocation: `https://${host}/${apiKey}.txt`,
        urlList: [postUrl.toString()]
    };

    const response = await fetch(indexNowUrl, {
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        method: 'POST'
    });

    if (!response.ok) {
        throw new Error(
            `Error submitting URL to IndexNow: ${response.statusText}`
        );
    }
    console.log(`URL ${post.url} successfully submitted to IndexNow`);
};

const posts = getNewPosts();
if (posts.length === 0) {
    console.log('No new posts to submit');
} else {
    const postRequests = [];
    for (const post of posts) {
        console.log(`Submitting updates for ${post.url}`);
        postRequests.push(
            postMastodonStatus(post),
            postBlueskyStatus(post),
            postIndexNow(post)
        );
    }

    const results = await Promise.allSettled(postRequests);
    for (const result of results) {
        if (result.status === 'rejected') {
            console.error(result.reason.message);
        }
    }
}
