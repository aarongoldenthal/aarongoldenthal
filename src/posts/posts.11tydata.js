import { fromZodError } from 'zod-validation-error';
import { z } from 'zod';

const descriptionMinLength = 100;
const descriptionMaxLength = 160;
const excerptMinLength = 200;
const excerptMaxLength = 650;
const titleMinLength = 20;
const titleMaxLength = 60;
// Requires `posts`, added below, plus one other tag
const minTags = 2;

const postsSchema = z.object({
    date: z.date(),
    description: z.string().min(descriptionMinLength).max(descriptionMaxLength),
    page: z.object({
        excerpt: z.string().min(excerptMinLength).max(excerptMaxLength)
    }),
    tags: z.array(z.string()).min(minTags),
    title: z.string().min(titleMinLength).max(titleMaxLength)
});

export default {
    eleventyDataSchema(data) {
        const result = postsSchema.safeParse(data);
        if (result.error) {
            throw fromZodError(result.error);
        }
    },
    layout: 'layouts/post.njk',
    tags: ['posts']
};
