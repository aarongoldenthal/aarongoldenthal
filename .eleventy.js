import fs from 'node:fs';
import path from 'node:path';

import { DateTime } from 'luxon';
import { EleventyHtmlBasePlugin } from '@11ty/eleventy';
import { imageSizeFromFile } from 'image-size/fromFile';
import markdownIt from 'markdown-it';
import markdownItAnchor from 'markdown-it-anchor';
import markdownItAttributes from 'markdown-it-attrs';
import { pathPrefix } from './src/_data/build.js';
import pluginNavigation from '@11ty/eleventy-navigation';
import pluginRss from '@11ty/eleventy-plugin-rss';
import pluginSyntaxHighlight from '@11ty/eleventy-plugin-syntaxhighlight';

// Global paths
const inputPath = 'src';
const outputPath = 'public';
// Assumes the image path is the same relative to
// the source and output directories
const imgPath = '/img/';

const imageShortcode = async (source, alt, style) => {
    // User provided paths from pages, only used to open image and get dimensions.
    // nosemgrep: path-join-resolve-traversal
    const imgSourcePath = path.join(inputPath, imgPath, source);
    // Don't use path.join to keep proper root URL syntax
    const imgOutputPath = `${imgPath}${source}`;

    if (!alt) {
        throw new Error(`Missing "alt" on image "${source}"`);
    }

    const { width, height } = await imageSizeFromFile(imgSourcePath);
    const styleString = style ? ` style="${style}"` : '';
    return (
        // Values are image path and dimensions, so do not include HTML.
        // nosemgrep: html-in-template-string
        `<img src="${imgOutputPath}" width="${width}" ` +
        `height="${height}" alt="${alt}" loading="lazy" ` +
        `decoding="async"${styleString}>`
    );
};

const sanitizeTag = (tag) =>
    tag.toLowerCase().replaceAll(/[#/]/g, '').replaceAll(' ', '-');

/* eslint-disable-next-line max-lines-per-function,
   jsdoc/require-jsdoc, unicorn/no-anonymous-default-export -- allow for config */
export default function (eleventyConfig) {
    // Copy static assets to the output directory
    eleventyConfig.addPassthroughCopy(path.join(inputPath, imgPath));
    // CSS is concatenated in a template, so watch for changes to source
    eleventyConfig.addWatchTarget(path.join(inputPath, 'css'));
    // JS is added to the page head, so watch for changes to source
    eleventyConfig.addWatchTarget(path.join(inputPath, 'js'));

    eleventyConfig.addPlugin(EleventyHtmlBasePlugin);
    eleventyConfig.addPlugin(pluginRss);
    eleventyConfig.addPlugin(pluginSyntaxHighlight, {
        preAttributes: { tabindex: 0 }
    });
    eleventyConfig.addPlugin(pluginNavigation);

    eleventyConfig.addShortcode('year', () => DateTime.now().toFormat('yyyy'));
    eleventyConfig.addShortcode('image', imageShortcode);

    // Formats date like "December 29, 2022", for most display
    eleventyConfig.addFilter('fullDate', (dateObject) =>
        DateTime.fromJSDate(dateObject, {
            zone: 'utc'
        }).toLocaleString(DateTime.DATE_FULL)
    );

    // Formats data per WHATWG standard. For feeds, <time> tags, etc.
    // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
    eleventyConfig.addFilter('htmlDateString', (dateObject) =>
        DateTime.fromJSDate(dateObject, {
            zone: 'utc'
        }).toFormat('yyyy-LL-dd')
    );

    // Get the first `n` elements of a collection.
    eleventyConfig.addFilter('head', (array, n) => {
        if (!Array.isArray(array) || array.length === 0) {
            return [];
        }
        if (n < 0) {
            return array.slice(n);
        }
        return array.slice(0, n);
    });

    // Return the smallest number argument
    eleventyConfig.addFilter('min', (...numbers) =>
        Math.min.apply(undefined, numbers)
    );

    // Return all the tags used in a collection
    eleventyConfig.addFilter('getAllTags', (collection) => {
        const tagSet = new Set();
        for (const item of collection) {
            for (const tag of item.data.tags || []) tagSet.add(tag);
        }
        return [...tagSet];
    });

    eleventyConfig.addFilter('filterTagList', (tags) =>
        (tags || []).filter(
            (tag) =>
                ![
                    'all',
                    'nav',
                    'post',
                    'posts',
                    'postsData',
                    'eleventy-plugin-validate'
                ].includes(tag)
        )
    );

    eleventyConfig.addFilter('stringify', (value) => JSON.stringify(value));

    eleventyConfig.addFilter('stringifyTags', (tags) =>
        JSON.stringify(tags.map((tag) => `#${sanitizeTag(tag)}`))
    );

    eleventyConfig.addFilter('renderHTML', (content = '') =>
        markdownIt({ html: true, linkify: true }).renderInline(content.trim())
    );

    // Create collection of posts data for use in external notifications
    eleventyConfig.addCollection('postsData', (collection) =>
        collection.getFilteredByTag('posts').map((item) => ({
            date: item.date,
            description: item.data.description,
            inputPath: item.inputPath,
            outputPath: item.outputPath,
            tags: item.data.tags.filter((tag) => tag !== 'posts'),
            title: item.data.title,
            url: item.url
        }))
    );

    // Add excerpt to page front matter is separator is present
    eleventyConfig.setFrontMatterParsingOptions({
        excerpt: true,
        // Optional, default is "---"
        // eslint-disable-next-line camelcase -- match API
        excerpt_separator: '<!-- excerpt -->'
    });

    // Move the posts.json file to the root folder since not deployed
    eleventyConfig.on('eleventy.after', () => {
        const filename = 'posts.json';
        // Filename hard-coded above.
        // nosemgrep: eslint.detect-non-literal-fs-filename
        fs.renameSync(path.join(outputPath, filename), filename);
    });

    // eslint-disable-next-line prefer-arrow-callback -- need it
    eleventyConfig.addTransform('highlight', function (content) {
        let newContent = content;
        const replacements = [
            {
                find: '˂',
                replace: '<span class="highlight">'
            },
            {
                find: '˃',
                replace: '</span>'
            },
            {
                find: '˄',
                replace: '<span class="highlight2">'
            }
        ];

        for (const replacement of replacements) {
            newContent = newContent.replaceAll(
                replacement.find,
                replacement.replace
            );
        }
        return newContent;
    });

    // Customize Markdown library and settings:
    eleventyConfig.amendLibrary('md', (mdLibrary) => {
        mdLibrary
            .use(markdownItAnchor, {
                // eslint-disable-next-line no-magic-numbers -- literals
                level: [1, 2, 3, 4],
                permalink: markdownItAnchor.permalink.linkInsideHeader({
                    class: 'header-link',
                    placement: 'after',
                    symbol: '#'
                }),
                slugify: eleventyConfig.getFilter('slug')
            })
            .use(markdownItAttributes, {
                allowedAttributes: ['id', 'class', 'style']
            });
        // Disable fuzzy linking (https://www.npmjs.com/package/markdown-it#linkify)
        mdLibrary.linkify.set({ fuzzyLink: false });
    });

    // Override @11ty/eleventy-dev-server defaults (used only with --serve)
    eleventyConfig.setServerOptions({
        showVersion: true
    });
}

export const config = {
    dir: {
        data: '_data',
        includes: '_includes',
        input: inputPath,
        output: outputPath
    },
    htmlTemplateEngine: 'njk',
    markdownTemplateEngine: 'njk',
    pathPrefix,
    templateFormats: ['md', 'njk', 'html']
};
