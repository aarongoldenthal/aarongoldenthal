---
title: Bin Tester v4.0.0 Released
description:
  Bin Tester v4.0.0 was released today with several breaking changes,
  including deprecating support for Node 14 and 19.
date: 2023-05-28
updated:
  - 2023-05-29
  - 2023-12-31
tags:
  - Node
  - Testing
---

Bin Tester v4.0.0 was released today with several noteworthy changes, including
changing the function exports, changing the argument to the test functions,
adding TypeScript type declarations, and deprecating support for Node 14 and 19.

<!-- excerpt -->

## Bin Tester overview

[Bin Tester](https://www.npmjs.com/package/bin-tester) provides a function to
simplify testing of a package's `bin` commands. It spawns a child process,
executes the command in a specific folder with specific arguments/environment
variables, and returns the results - exit code, any `Error` thrown, and the full
text of `stdout` and `stderr`.

## Changes in v4.0.0

There are three noteworthy changes in v4.0.0, all of which could be breaking.
Please see
[the release notes](https://gitlab.com/gitlab-ci-utils/bin-tester/-/releases/4.0.0)
for other minor changes.

### Exported test function and argument changes (BREAKING)

Previously the test function used the default export, but it was moved to the
named `bin` export to allow for future expansion of the package. The `bin`
function also now accepts a single `options` object argument with properties for
all of the previous arguments.

The following is an example test `jest` with the previous API. As can be seen,
the number possible of arguments has gotten long over time, which makes it
difficult to specify a subset of the arguments, and unclear which arguments are
actually required for test.

```js
// bin-tester@3.0.1
const bin = require('bin-tester');

it('should successfully execute and output JSON results to stdout if JSON format specified', () => {
    const testArgs = ['-f', 'json'];
    const commandName = 'my-command';
    // Previously API was:
    //   bin(binArguments, workingDirectory, packageDirectory, environment, commandName, timeout)
    const results = await bin(testArgs, './', './', {}, commandName);

    expect(results.code).toBe(0);
    expect(results.stderr).toBe('');
    expect(results.stdout).toMatchSnapshot();
});

```

To update for the change in v4.0.0:

- The `bin` function is now required/imported as a named export.
- The required `bin` function arguments should be encapsulated in one `options`
  object.

These changes are shown in the example below. In this case only the two required
arguments are specified (as opposed to the 5 in the preceding example).

```js
// bin-tester@4.0.0
const { bin } = require('bin-tester');

it('should successfully execute and output JSON results to stdout if JSON format specified', () => {
    const options = { binArguments: ['-f', 'json'], command: 'my-command' };
    const results = await bin(options);

    expect(results.code).toBe(0);
    expect(results.stderr).toBe('');
    expect(results.stdout).toMatchSnapshot();
});
```

The complete list of properties for the `options` object can be found
[in the README](https://gitlab.com/gitlab-ci-utils/bin-tester#usage).

### Added TypeScript type declarations (BREAKING)

The package now contains built-in TypeScript type declarations, part of a bigger
effort to add types for all of my library-type packages. This is only BREAKING
if you have implemented custom types for this package.

### Deprecated support for Node 14 and 19 (BREAKING)

Support for Node 14 (which went end of life on April 30, 2023) and Node 19
(which goes end of life on June 1, 2023) has been deprecated. The package
continues to be tested against all current and LTS releases
(`^16.13.0 || ^18.12.0 || >=20.0.0`). Once Node releases are end of life, any
identified vulnerabilities are not patched, and this is intended to actively
discourage keeping unsupported version of Node in use.
