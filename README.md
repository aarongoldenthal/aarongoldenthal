## Where to find me

- My open source projects: https://gitlab.com/gitlab-ci-utils
- My blog: https://aarongoldenthal.com/
- My GitHub profile: https://github.com/aarongoldenthal/ (not a lot there)
