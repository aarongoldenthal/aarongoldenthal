---
layout: layouts/home.njk
title: About Me
templateClass: post
date: 2023-01-10
updated:
  - 2023-12-31
eleventyNavigation:
  key: About
  order: 3
---

<h1>{{ title }}</h1>

I started writing software at a young age on a shiny, new (and I assume insanely
expensive) Apple IIe in the mid 80s. In college I studied Aerospace Engineering,
and most of my software development work was in
[Computational Fluid Dynamics (CFD)](https://en.wikipedia.org/wiki/Computational_fluid_dynamics),
which was the focus of my Master's degree. While in college, working on what I'm
sure was a very important project, a friend came by and showed me something that
would literally change the world -
[NCSA Mosaic](<https://en.wikipedia.org/wiki/Mosaic_(web_browser)>) - the first
browser for the World Wide Web. I've been involved in some form of Web or other
software development since then.

I've worked on software of many kinds - Microsoft Office macros, Web
applications of various forms through several decades, Web browser extensions,
Windows thick client applications, and many others up to and including ground
and onboard software to control manned spacecraft.

Over that time I've worked on, or with, a long list of languages, frameworks,
and technologies, including (in _mostly_ chronological order):

> BASIC, Pascal, Fortran, C, C++, HTML, JavaScript/ECMAScript, CSS, Perl/CGI,
> VBScript/Classic ASP, SQL, [HAL/S](https://en.wikipedia.org/wiki/HAL/S),
> [Ada](<https://en.wikipedia.org/wiki/Ada_(programming_language)>), VBA,
> ASP.NET, XML/XSD/XSLT, VB.NET, C#, Python, JavaScript/ECMAScript (_again_),
> web browser extensions, HTML (_again_), Node.js, Docker, Kubernetes, JSON
> Schema, shell/Bash, PowerShell, TypeScript,
> [Rockstar](https://codewithrockstar.com/), CSS (_again_), Go

These days I focus on open source projects, mostly automation and test tools.
They're usually written in Node.js, sometimes in Bash, and almost exclusively
running in containers. I use GitLab and GitLab CI, which I firmly believe is a
better software development platform that GitHub (although I'll admit GitHub
does have some better collaboration tools). I'm also a devout DevOps advocate
and practitioner of continuous integration, continuous delivery, and continuous
deployment (and yes, those are three distinct things).
