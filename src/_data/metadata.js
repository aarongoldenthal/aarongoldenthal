const description =
    'I write about software development, continuous integration/delivery, ' +
    'GitLab, containers, Node.js, and numerous other topics.';
const name = 'Aaron Goldenthal';
const url = 'https://aarongoldenthal.com/';

const metadata = {
    author: {
        email: 'blog@aarongoldenthal.com',
        name,
        url: `${url}about/`
    },
    description,
    img: 'https://secure.gravatar.com/avatar/be58dbc60e931e2241e586435025900c?s=48&d=identicon',
    language: 'en',
    title: name,
    url
};

export default metadata;
