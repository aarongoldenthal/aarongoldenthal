import gitlabEnv from 'gitlab-ci-env';

const defaultBuildType = 'test';
const defaultPathPrefix = '/';
const defaultEnvironment = 'test';

// Production build must be specified, otherwise assume test
export const buildType = process.env.BUILD_TYPE || defaultBuildType;

// Review apps job is only run for merge request pipelines, so
// don't need to check branch.
export const isReviewApp = gitlabEnv.ci.isCI && gitlabEnv.ci.mergeRequest.id;

// If buildType is not "pages", build is for test and use the default
// pathPrefix. If the buildType is "pages", but not a review app, build is
// for production and use the default pathPrefix. If the buildType is
// "pages" and is a review app, update path prefix to job artifacts location
// to properly resolve URLs that reference the root folder (since nested).
export const pathPrefix =
    buildType === 'pages' && isReviewApp
        ? `/-/${gitlabEnv.ci.project.name}/-/jobs/${gitlabEnv.ci.job.id}/artifacts/public/`
        : defaultPathPrefix;

// Include analytics if buildTypes is "pages" and on the default branch
export const includeAnalytics =
    buildType === 'pages' &&
    gitlabEnv.ci.commit.branch === gitlabEnv.ci.defaultBranch;

export const environment = gitlabEnv.ci.environment.name || defaultEnvironment;
