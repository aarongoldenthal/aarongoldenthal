---
title: GitLab Releaser v5.0.0 Released
description:
  GitLab Releaser v5.0.0 was released today with several breaking changes,
  including failing for an empty release description from the CHANGELOG.
date: 2023-01-30
updated:
  - 2023-12-31
tags:
  - CI/CD
  - GitLab
  - Node
---

GitLab Releaser v5.0.0 was released today with several noteworthy changes,
including failing if an empty release description is pulled from the CHANGELOG
and a new CLI option to specify the CHANGELOG path.

<!-- excerpt -->

## GitLab Releaser overview

[GitLab Releaser](https://www.npmjs.com/package/gitlab-releaser) takes a JSON
file with details for a release and generates a shell script to leverage
GitLab's [`release-cli`](https://gitlab.com/gitlab-org/release-cli/) application
to create a release in the GitLab project. It can be configured to pull the
release name and description (release notes) from the CHANGELOG. GitLab's
`release-cli` application is used to simplify variable expansion, and with the
expectation that it will be
[updated to accept this JSON format](https://gitlab.com/gitlab-org/release-cli/-/issues/50)

## Changes in v5.0.0

There are three noteworthy changes in v5.0.0, two of which could be breaking.
Please see
[the release](https://gitlab.com/gitlab-ci-utils/gitlab-releaser/-/releases/5.0.0)
for other minor changes.

### Fail on empty CHANGELOG description (BREAKING)

Previously, if the release description was taken from the CHANGELOG and GitLab
Releaser found an empty description, that was used when creating the release,
resulting in no release notes. History has shown that when this occurs it's
almost always a formatting error in the CHANGELOG, something like:

```md
## v2.1.0

## Changed

- Update a great feature to be even better...
```

In this case, when release `4.0.4` is found, GitLab Releaser recognizes that
releases use a `##` header, so the next release is assumed to start at
`## Changed`, and an empty description is returned. The correct CHANGELOG for
this example is:

```md
## v2.1.0

### Changed

- Update a great feature to be even better...
```

With the previous behavior the release was created, and the release notification
sent out, with no release notes. To avoid this, the default behavior was changed
to note the error and fail if this occurs.

If the previous behavior is desired, there is a new CLI option
`--allow-empty-changelog-desc` (`-a`) to allow an empty CHANGELOG description.

### Specifying CHANGELOG path or file

Previously, if the release name or description was taken from the CHANGELOG,
GitLab Releaser assumed the CHANGELOG was located in the project root. While
this is frequently the case, it is not always. There are also cases where the
logic to select the CHANGELOG can fail or select the wrong file, especially if
there are multiple CHANGELOG files or it has an atypical name.

There is now a CLI option `--changelog` (`-c`) to be able to specify either the
path to the CHANGELOG using the existing logic to locate the file, or specify
the actual path.

### Programmatic API change (BREAKING)

With multiple new options added, the `getReleaseCliCommand` function was
refactored to accept an `options` object with all CLI options. This is only an
impact if that function is used programmatically (which is atypical).
