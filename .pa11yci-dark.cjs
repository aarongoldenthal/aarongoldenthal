'use strict';

/* eslint-disable-next-line node/no-missing-require -- puppeteer installed in
   CI job */
const puppeteer = require('puppeteer');

const getContext = async () => {
    const browser = await puppeteer.launch({});
    const page = await browser.newPage();
    await page.emulateMediaFeatures([
        { name: 'prefers-color-scheme', value: 'dark' }
    ]);
    return { browser, page };
};

const context = getContext().then((c) => c);

module.exports = {
    defaults: {
        browser: context.browser,
        ignore: ['color-contrast', 'frame-tested'],
        'ignore-description': {
            'color-contrast': 'reports false positive with background-image',
            'frame-tested': 'simply notes that iframes should be tested'
        },
        page: context.page,
        reporters: [
            'pa11y-ci-reporter-cli-summary',
            ['json', { fileName: './pa11y/pa11y-ci-results.json' }],
            ['pa11y-ci-reporter-html', { destination: './pa11y' }]
        ],
        runners: ['htmlcs', 'axe']
    }
};
