import esmConfig from '@aarongoldenthal/eslint-config-standard/esm-config.js';
import globals from 'globals';
import recommendedConfig from '@aarongoldenthal/eslint-config-standard/recommended.js';

esmConfig.languageOptions.globals = {
    ...globals.browser
};
esmConfig.rules = {
    // Browser code
    'node/no-unsupported-features/node-builtins': 'off'
};

const config = [
    ...recommendedConfig,
    esmConfig,
    {
        ignores: [
            '!.*.js',
            '.vscode/**',
            'archive/**',
            'node_modules/**',
            'coverage/**'
        ],
        name: 'ignores'
    }
];

export default config;
