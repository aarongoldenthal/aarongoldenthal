---
title: The Blog Is Back In Town
description:
  It's been well over a decade since I kept a blog here, a lifetime or two in
  software development, but I've missed writing and decided to bring it back.
date: 2023-01-10
updated:
  - 2024-01-07
tags:
  - 'State of the Blog'
---

It's been well over a decade since I kept a blog here. That's a lifetime or two
in software development, but I've missed writing and decided it's time to bring
it back. So, this is the kick-off to incentivize me to write more. And it never
hurts to have another new project to work on, and something with more web
front-end work, that is developing a site with
[Eleventy](https://www.11ty.dev/), hosted on GitLab pages, optimizing the use of
GitLab's tooling, and extensive use of automation with GitLab CI.

<!-- excerpt -->

This blog will focus on the software world I live in today and my open source
work, which includes:

- Continuous integration, continuous delivery, continuous deployment, and
  automation
- Leveraging GitLab and GitLab CI to optimize software development
- Applications written in Node.js, sometimes Bash, and usually run in containers
- Development of automation and automated test tools

Most of my open source projects are hosted in GitLab in the group
[GitLab CI Utils](https://gitlab.com/gitlab-ci-utils) - a mix of GitLab CI jobs,
containers, and Node.js tools/libraries. This blog's site lives at my
[GitLab.com profile](https://gitlab.com/aarongoldenthal), but there's not a lot
else there (at least not public). Most of my projects are maintained in a
separate group to facilitate participation in
[GitLab's Open Source Program](https://about.gitlab.com/solutions/open-source/join/),
which provides free GitLab Ultimate licenses, and the additional capabilities
that brings, to improve open source development.

I do also have a [GitHub profile](https://github.com/aarongoldenthal), but this
is primarily forks of various open source projects that I contribute to. Many
are one-off contributions, but I do make frequent contribution to
[Pa11y](https://github.com/pa11y/pa11y) and
[Pa11y CI](https://github.com/pa11y/pa11y-ci) (automated web accessibility
testing tools).

## Blog history

My last blog was written 13 - 15 years ago. At that point it was focused on web
development with ASP.NET. So, for a little entertainment here are some stats on
the state of web development at that time to show how far things have come since
then.

### Browsers

- Internet Explorer still had 50% of the browser market share, and even though
  Internet Explorer 9 was the latest release, most web applications were still
  written to support Internet Explorer 6/7/8 (that is, before the DOM was
  standardized).
- Chrome v5 was the latest release, and it was still using WebKit (yes, that
  WebKit).

### Web applications

- ASP.NET v4.0, using Web Forms, was just released. ASP.NET MVC was a release
  candidate and .Net Core was still 6 years away. Enterprises not using ASP.NET
  were probably using ColdFusion or Classic ASP.
- There was no such thing as front-end, back-end, or full-stack web developers.
  We were just web developers, and we were all full-stack.
- The terms multi-page app (MPA), server-side rendering (SSR), and file system
  based routing didn't exist because all web apps were server-side rendered
  multi-page apps using file system based routing (maybe with a little AJAX
  sprinkled in).
- Many developers still expected HTML to evolve to some form of XHTML strict.
  The WHATWG had formed and created an initial HTML 5 draft spec, but it
  wouldn't be a candidate recommendation for 2 years, and no browser would fully
  support it for 3 years.
- REST was just starting to get real adoption as a successor to SOAP for web
  services.

### Front end

- It took some courage (and probably a lot of wasted time) to develop
  cross-platform or cross-browser web applications without jQuery.
- Many designs (especially some older enterprise apps) still conformed to the
  "web safe color palette."
- Responsive web design was just starting to be used, and Twitter had just
  released the Bootstrap framework to make it practical.
- AngularJS came out late that year (that is v1, before v2 was re-named as
  Angular). The first release of Typescript was still 2 years away. The first
  release of React was 3 years away.
