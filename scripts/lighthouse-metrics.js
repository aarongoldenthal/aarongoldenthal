/**
 * @module lighthouse-metrics
 *
 * Module to post-process a Lighthouse JSON report and generate a metrics
 * report with metrics for each category and form factor tested.
 */

import fs from 'node:fs';

const formFactor = process.env.FORM_FACTOR || '';
const scoreScalingFactor = 100;
const reportFileName = `lighthouse-${formFactor}.report.json`;
const metricsFileName = 'lighthouse-metrics.txt';
const categories = ['performance', 'accessibility', 'best-practices', 'seo'];

// Reading value based on user input
// nosemgrep: eslint.detect-non-literal-fs-filename
const results = JSON.parse(fs.readFileSync(reportFileName, 'utf8'));
const metrics = categories
    .map(
        (category) =>
            `lighthouse{category="${category}",form_factor="${formFactor}"} ${
                // Object notation used with fixed values
                // nosemgrep: eslint.detect-object-injection
                results?.categories[category]?.score * scoreScalingFactor
            }`
    )
    .join('\n');
fs.writeFileSync(metricsFileName, metrics);
