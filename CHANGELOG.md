# Changelog

## v2.1.2 (2025-02-26)

### Fixed

- Updated to `image-size@2.0.0` and refactored code for breaking changes.
- Update blog vocabulary for new posts.

## v2.1.1 (2025-01-05)

### Fixed

- Updated post `description`, `title`, and `excerpt` metadata checks to include
  both a minimum and maximum length.

## v2.1.0 (2025-01-01)

### Changed

- Added link in footer to Bluesky profile. (#106)

### Fixed

- Updated `engines:node` to `^22`, which is the active LTS release.
- Updated `lighthouse` analysis to skip the `bf-cache` audit, which was crashing
  Chrome.
- Update Vale vocabulary per latest posts.
- Update `pagean` analysis to allow failure for the broken link check. With the
  latest posts there are a lot of GitLab links, and in some cases it return 429
  response codes.

## v2.0.3 (2024-10-14)

### Fixed

- Updated to `gitlab-pa11y-ci:10.0.4` to resolve issue with `pa11y_ci_dark` job
  hanging after analysis was complete. (#103)

## v2.0.2 (2024-10-09)

### Fixed

- Added `posts` validation check for `page.excerpt` to ensure post list renders
  properly. (#104)

## v2.0.1 (2024-10-08)

### Fixed

- Fixed Giscus theme being improperly set with feature flag. (#102)

## v2.0.0 (2024-10-07)

### Changed

- Updated to Eleventy v3 and converted to ESM. Eliminated plugins
  `eleventy-plugin-blog-tools` and `eleventy-plugin-validate` in favor of native
  features.

### Fixed

- Update all other `devDependencies` to latest versions.
- Pinned `lighthouse` version in CI jobs.

### Miscellaneous

- Upgraded to ESLint v9.

## v1.17.0 (2024-04-18)

### Changed

- Added a splash screen tied to the Mercedes theme feature flag that scrolls
  on-screen, then theme changes, then scrolls off. (#101)

## v1.16.3 (2024-04-08)

### Fixed

- Updated the Mastodon post status to include the post description to match
  Bluesky.
- Updated `pa11y_ci_dark` CI job to pass page configured for dark mode to
  properly test dark mode accessibility. (#95)
- Updated posts list to only display post title since `eleventy-plugin-validate`
  schema ensures all `posts` have a `title`.
- Cleaned up `eleventy.after` event to avoid duplicate data.
- Updated posts styles to hide header links except on hover for improved
  accessibility.
- Fixed formatting issues with `<ul>` tags.
- Updated Blog vocabulary for new posts.

## v1.16.2 (2024-03-17)

### Fixed

- Updated CI pipeline to save `posts.json` from `pages` job, not
  `build_test_site`.
- Added `stringify` shortcode to optimize JSON serialization and updated
  `posts.json` output.

## v1.16.1 (2024-03-16)

### Fixed

- Fixed color contrast issue with comments in highlighted code.

## v1.16.0 (2024-03-11)

### Changed

- Added capability to highlight arbitrary code blocks with `highlight`
  transform.
- Added capability to add responsive side-by-side code blocks with:

```html
<div class="code-side-by-side"><div>
...
</div><div>
...
</div></div>
```

## v1.15.1 (2024-03-10)

### Fixed

- Updated new posts script to use `POST` method and post to IndexNow directly
  for improved feedback. (#99)

## v1.15.0 (2024-02-18)

### Changed

- Updated new posts script to post status to Bluesky. (#98)

### Fixed

- Fixed new post logic so all errors are written to stderr.

### Miscellaneous

- Updated Renovate config to use use new
  [presets](https://gitlab.com/gitlab-ci-utils/renovate-config) project. (#97)

## v1.14.1 (2024-02-11)

### Fixed

- Moved Bing API file generation from `build_test_site` to `pages` job.

## v1.14.0 (2024-02-10)

### Changed

- Update build, scripts, and CI jobs to post to Mastodon and Bing IndexNow for
  new `posts`. (#43)

### Miscellaneous

- Move CI scripts from `.gitlab` to `scripts` directory.
- Update Vale `Blog` vocabulary.

## v1.13.0 (2024-01-22)

### Changed

- Increased most font sizes to improve readability.

### Fixed

- Update `lint_prose` to use template and update vocabulary.
- Update `eleventy-plugin-validate` collection name for v0.1.2.

## v1.12.1 (2024-01-07)

### Fixed

- Fixed the tags page and filtered tags collection to exclude the
  `__eleventy-plugin-validate` collection.

## v1.12.0 (2024-01-07)

### Changed

- Implemented `posts` data validation via
  [`eleventy-plugin-validate`](https://www.npmjs.com/package/eleventy-plugin-validate).
  (#91)

### Fixed

- Updated to `stylelint@16`.

### Miscellaneous

- Updated `lint_prose` job to use
  [`vale` image](gitlab.com/gitlab-ci-utils/container-images/vale) and removed
  custom content now contained in the image.

## v1.11.1 (2024-01-01)

### Fixed

- Updated Vale config to ignore Nunjucks shortcodes. (#94)
- Updated the Vale fingerprints with the check so that each line is not unique,
  but each check on each line is. Previously multiple findings on the same line
  were tracked as a single finding.

## v1.11.0 (2023-12-31)

### Changed

- Setup [`vale`](https://vale.sh/) to lint prose. (#88)
- Updated `imageShortcode` to allow optional `style` attribute.
- Updated markdown library to allow adding `id`, `class`, or `style` to elements
  with [`markdown-it-attrs`](https://www.npmjs.com/package/markdown-it-attrs).

## v1.10.3 (2023-11-17)

### Fixed

- Updated page base font to use default browser size.
- Updated all images to use shortcode that adds `width` and `height` attributes.
  (#89)
- Tweaked light theme background colors and link underline offset.

## v1.10.2 (2023-11-10)

### Fixed

- Updated engine to Node 20, now the active LTS version.
- Fixed light theme post card box shadow.
- Fixed link underline offset.

## v1.10.1 (2023-09-04)

### Fixed

- Fixed dark theme box shadow color.

## v1.10.0 (2023-07-13)

### Changed

- Updated Pagean config to use sitemap, now supported in v9.0.0. Also updated
  job to use `serve` instead of `http-server`, which has been removed.

## v1.9.2 (2023-06-30)

### Fixed

- Updated CSS to use link underline instead of bottom border. (#81)

## v1.9.1 (2023-04-30)

### Fixed

- Replaced `markdownlint-cli` with `markdownlint-cli2`.
- Updated SAST jobs to always save artifacts.
- Cleanup up unused `depcheck` ignores.
- Added `eslint` ESM config overrides to website JavaScript. The `eleventy` code
  is still CJS.

## v1.9.0 (2023-04-16)

### Changed

- Added blog commments using [Giscus](https://giscus.app/). (#34)

### Fixed

- Update `pa11y-ci` configs to ignore the Axe `frame-tested` rule, which is a
  notification to check iframes.
- Update all CSS `em`s to `rem`s for consistency.

## v1.8.1 (2023-04-05)

### Fixed

- Removed unused OWASP Dependency Check suppressions. (#77)
- Tweaked accent background color.

## v1.8.0 (2023-03-19)

### Changed

- Updated `pages` job with explicit job `needs` for efficiency. (#75)
- Update to job templates v16, adding `socket` analysis for merge request
  pipelines with dependency changes.

## v1.7.0 (2023-03-18)

### Changed

- Updated GitLab security jobs (SAST, Secret Detection) to fail on any findings.
  (#74)

## v1.6.0 (2023-03-16)

### Changed

- Added header logo, including alternate for GitLab feature flag theme. (#72)

### Fixed

- Disabled CI pipelines for tegs. (#73)

## v1.5.0 (2023-03-05)

### Changed

- Added capability to set CSS theme via GitLab feature flag. (#70)

### Miscellaneous

- Added `osv_scanner` job to CI pipeline.
- Update to CI templates v15.3.0, fixing `code_count` for Nunjucks.

## v1.4.0 (2023-02-12)

### Changed

- Updated to final Eleventy v2.0.0 release and tracking `latest` tag.
- Updated `prism` theme to Night Owl. (#68)

### Fixed

- Fixed post header CSS to properly wrap tags on mobile.
- Fixed page nav CSS to use semantic tags for highlighting current page.

## v1.3.1 (2023-01-31)

### Fixed

- Updated Google font link to avoid flash of unstyled text, then reverted to
  system fonts.

## v1.3.0 (2023-01-22)

### Changed

- Updated to Eleventy v2 (#51)
- Added custom image shortcode with image dimensions. (#62)

## v1.2.1 (2023-01-21)

### Fixed

- Cleaned up header, blockquote, and code formatting. (#60, #63)
- Update `code` and `blockquote` for mobile layout. (#64)

## v1.2.0 (2023-01-15)

### Changed

- Added Lighthouse analysis (desktop and mobile configurations) to CI pipeline,
  with JavaScript module to extract metrics report with Prometheus-style metrics
  for all categories and form factors. (#54)

## v1.1.0 (2023-01-13)

### Changed

- Added deployment environment for pages job to track production releases. (#46)
- Updated review apps to post-process links to point to files instead of
  folders. (#42)
- Added Google analytics to all pages, but only if a production pages
  deployment. (#11)

## v1.0.0 (2023-01-10)

Initial blog release

## v0.1.0 (2019-10-05)

Initial placeholder release
