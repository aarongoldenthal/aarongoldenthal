const delay = (ms) =>
    /* eslint-disable-next-line promise/avoid-new,
       no-promise-executor-return -- Required for delay */
    new Promise((resolve) => setTimeout(resolve, ms));

const displaySplash = async (splash) => {
    splash.style.inlineSize = '100%';
    const delayTime = 500;
    await delay(delayTime);
};

const hideSplash = async (splash) => {
    const delayTime = 1000;
    await delay(delayTime);
    splash.style.cssText =
        'inline-size: 0; inset-inline-start: auto; inset-inline-end: 0;';
};

const themeKey = 'theme';
const savedTheme = sessionStorage.getItem(themeKey);
let giscusTheme = 'light';
if (savedTheme) {
    document.body.classList.add(savedTheme);
    giscusTheme = 'dark';
}

const featureFlagConfig = {
    appName: '{{ build.environment }}',
    instanceId: 'zHbLkmZF-rjjkJUmbKa-',
    url: 'https://gitlab.com/api/v4/feature_flags/unleash/14670817/client/features/'
};

try {
    const response = await fetch(featureFlagConfig.url, {
        headers: {
            'UNLEASH-APPNAME': featureFlagConfig.appName,
            'UNLEASH-INSTANCEID': featureFlagConfig.instanceId
        }
    });
    const featureData = await response.json();
    const themeFeature = featureData?.features.find(
        (feature) => feature.name === 'theme_mercedes'
    );

    const splashScreen = document.querySelector('#splash');
    if (themeFeature?.enabled && !savedTheme) {
        await displaySplash(splashScreen);
        document.body.classList.add('mercedes');
        await hideSplash(splashScreen);

        sessionStorage.setItem(themeKey, 'mercedes');
        giscusTheme = 'dark';
    } else if (!themeFeature?.enabled && savedTheme) {
        await displaySplash(splashScreen);
        document.body.classList.remove('mercedes');
        await hideSplash(splashScreen);

        sessionStorage.removeItem(themeKey);
        giscusTheme = 'light';
    }
} catch {
    console.error('Error processing feature flags');
}

try {
    const giscusConfig = {
        async: '',
        crossorigin: 'anonymous',
        'data-category': 'Announcements',
        'data-category-id': 'DIC_kwDOIydnus4CTogU',
        'data-emit-metadata': 0,
        'data-input-position': 'bottom',
        'data-lang': 'en',
        'data-loading': 'lazy',
        'data-mapping': 'pathname',
        'data-reactions-enabled': 0,
        'data-repo': 'aarongoldenthal/blog-comments',
        'data-repo-id': 'R_kgDOIydnug',
        'data-strict': 1,
        'data-theme': giscusTheme,
        src: 'https://giscus.app/client.js'
    };

    const article = document.querySelector('main.post article');
    if (article) {
        const giscusScript = document.createElement('script');
        for (const [key, value] of Object.entries(giscusConfig)) {
            giscusScript.setAttribute(key, value);
        }
        article.append(giscusScript);
    }
} catch {
    console.error('Error adding comments');
}
